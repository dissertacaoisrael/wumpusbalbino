package wumpusworld;

import javax.swing.SwingUtilities;

import wumpusworld.gui.MainWindow;

public class Main {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainWindow s = new MainWindow();
				s.setVisible(true);
			}
		});
	}
}
