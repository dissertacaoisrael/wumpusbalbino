package wumpusworld.util;

public enum Direction 
{
	NORTH,
	SOUTH,
	EAST,
	WEST;
}
