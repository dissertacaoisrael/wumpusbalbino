package balbinosaloon.objects;

public enum BeerState 
{
	WARM,
	ICED;
}
